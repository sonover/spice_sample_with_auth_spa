const gulp = require('gulp');
const watch = require('gulp-watch');
const batch = require('gulp-batch');
const connect = require('gulp-connect');
const copy = require('gulp-copy');
const babel = require('gulp-babel');
const clean = require('gulp-rimraf');
const path = require('path');
let paths = {};
paths.koa = './core/server/**/*.js';
paths.koa_routes = './core/client/routes/**/*.js';
paths.koa_config = './config/**/*.js';
const spawn = require('child_process').spawn;
let node;

gulp.task('clean_routes', [], function () {
    console.log("Clean all files in  routes build folder");
    return gulp.src("build/routes/*", {read: false}).pipe(clean());
});

gulp.task('clean_config', [], function () {
    console.log("Clean all files in config build folder");
    return gulp.src("build/config/*", {read: false}).pipe(clean());
});

gulp.task('build_koa', () => {
    console.log('Building Koa')
    return gulp.src('core/server/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('build_routes', ['clean_routes'], () => {
    console.log('Building Routes');
    return gulp.src('core/client/routes/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build/routes'));
});

gulp.task('build_config', ['clean_config'], () => {
    console.log('Building  Config');
    return gulp.src('config/**')
        .pipe(babel({
            presets: ['es2015', 'stage-0']
        }))
        .pipe(gulp.dest('build/config'));
});


gulp.task('server', function () {
    if (node) node.kill();
    node = spawn('node', ['build/index.js'], {stdio: 'inherit'})
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
});


// Default task
gulp.task('default', ['build']);
gulp.task('build', ['build_koa', 'build_routes', 'build_config']);
gulp.task('serve', ['build', 'server']);
gulp.task('watch-server', function () {
   // gulp.start('serve');
    gulp.watch(paths.koa_config, ['build_config', 'server']);
    gulp.watch(paths.koa_routes, ['build_routes', 'server']);
    gulp.watch(paths.koa, ['build_koa', 'server']);
    //gulp.watch(paths.client, ['serve']);
});