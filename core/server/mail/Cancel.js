"use strict";
const driver_options = {};
const template = "";
const driver_name = "";
export default class Cancel extends spice.classes.Mail {
    constructor(args) {
        if(!args.template){
            args.template = template;
        }

        if(!args.driver_name){
            args.driver_name = driver_name;
        }
       
        if(!args.driver_options){
            args.driver_options = driver_options;
        }
        super(args)
    }
}