"use strict";
let Rules = spice.classes.validate.Rules;
import User from "../models/User";

Rules.prototype.emailIsUnique = async function(field, value, message){
    try{
        var user = new User();
        try{
            var user_found = await user.getByEmail({email: value});
            this.validator.addError(field, 'rule', 'emailIsUnique', message || 'This email is already in use');
            return false;
        }catch(e){
            return true;
        }
    }catch(e){
        this.validator.addError(field, 'rule', 'emailIsUnique', 'Cannot Validate email');
        return false;
    }
}

Rules.prototype.emailMustMatchUser = async function(field, value, message){
    try{
        var user = new User();
        let user_found = await user.getByEmail({email: value});
        if(user_found){
            return true;
        }else{
            throw new Error();
        }

    }catch(e){
        if(e.message == 'user does not exist'){
            this.validator.addError(field, 'rule', 'emailMustMatchUser', message || 'This email does not match any user');
            return false;
        }else{
            this.validator.addError(field, 'rule', 'emailMustMatchUser', 'Cannot Validate email');
            return false;
        }
    }
}
