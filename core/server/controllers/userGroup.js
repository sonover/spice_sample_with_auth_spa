"use strict";
import UserGroup from '../models/UserGroup';
import EntityPermission from '../models/EntityPermission';
import Permission from '../models/Permission';
import co from 'co';
import _ from 'lodash';

let utility = spice.classes.RestHelper;

var object = {
	get: async function(ctx,next){
		try{
			var userGroup = new UserGroup();
			ctx.body = utility.prepare_response(utility.SUCCESS,await userGroup.get({id:ctx.params.user_group_id}));
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	list: async function(ctx,next){
		try{
			var userGroup = new UserGroup();
			ctx.body = utility.prepare_collection(utility.SUCCESS, await userGroup.list(ctx.query));
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	get_permissions: async function(ctx,next){
		try{
            var userGroup = new UserGroup();
            let group = await userGroup.get({id:ctx.params.user_group_id});
			ctx.body = utility.prepare_collection(utility.SUCCESS,group.permissions);
		}catch(e){
			console.log(e);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	add_permission: async function(ctx,next){
		try{
            var userGroup = new UserGroup();
            let group = await userGroup.get({id:ctx.params.user_group_id});
            group.permissions = _.map(group.permissions, o=>{
            	return o.id;
			})
            group.permissions = _.union(group.permissions,[ctx.params.permission_id]);
            console.log("Groups with permission ids", group);
            var userGroup1 = new UserGroup(group);
            let updated_group = await userGroup1.update({id:ctx.params.user_group_id});
			ctx.body = utility.prepare_response(utility.SUCCESS,updated_group.permissions);
		}catch(e){
			console.log(e);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	remove_permission: async function(ctx,next){
		try {
			console.log("User Group");
            var userGroup = new UserGroup();

            let group = await userGroup.get({id:ctx.params.user_group_id});

            group.permissions = _.map(group.permissions, o=>{
                return o.id;
            })
            console.log("Group Permissions Before remove",group.permissions, ctx.params.permission_id);
            _.remove(group.permissions, o=>{return o == ctx.params.permission_id});
            console.log("Group Permissions After remove",group.permissions, ctx.params.permission_id);
            var userGroup1 = new UserGroup(group);
            ctx.body = utility.prepare_response(utility.SUCCESS,await userGroup1.update({id:ctx.params.user_group_id}));
		}catch(e){
			console.log(e.stack);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	update: async function(ctx,next){
		try{
			var userGroup = new UserGroup(ctx.request.body);
			ctx.body = utility.prepare_response(utility.SUCCESS,await userGroup.update({id:ctx.params.user_group_id}));
		}catch(e){
			console.log(e);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	create: async function(ctx,next){
		try{
			var userGroup = new UserGroup(ctx.request.body);
			ctx.body = utility.prepare_response(utility.SUCCESS,await userGroup.create({id_prefix:'user_group'}));
		}catch(e){
			console.log(e.stack);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	delete: async function(ctx,next){
		try{
			var userGroup = new UserGroup();
			ctx.body = utility.prepare_response(utility.SUCCESS,await userGroup.delete({id:ctx.params.user_group_id}));
		}catch(e){
			console.log(e);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},
	
	migrate: async function(ctx,next){
		
	},

	validate_insert: async function(ctx,next){
		console.log(ctx.request.body)
		var rules = {
			name: 'required',
		};
		var messages = {

		};
		var filters = {
			before: {
				id: "lowercase|trim",
				name: "trim"
			}
		}

		await co(ctx.validateBody(rules, messages, filters));

		if (ctx.validationErrors) {
			ctx.status = 422;
			ctx.body = ctx.validationErrors;
		} else {
			await next();
		}
	}
}

module.exports = object;