"use strict";
import  Permission from '../models/Permission';
import  User from '../models/User';
import  UserGroup from '../models/UserGroup';
import EntityPermission from '../models/EntityPermission';
var _ = require('lodash');
let utility = spice.classes.RestHelper;

var object = {
	check_user_exist: async function(ctx, next){
		try{
			let user = ctx.state.user;
			//console.log(user);
			let user_obj = new User();
            let response = await user_obj.get({id:user.id})
            ctx.state.user_obj = response;
            ctx.state.permissions = response.group.permissions;
			await next();
		}catch(e){
			console.log(e)
			ctx.status = 403;
			ctx.body = utility.prepare_response(utility.FAILURE,"Unauthorized");
		}
	}
}

module.exports = object;