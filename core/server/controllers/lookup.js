"use strict";
import Lookup from '../models/Lookup';
import _ from 'lodash';
let utility = spice.classes.RestHelper;
import co from 'co';

var object = {
    get: async function(ctx, next){
        try{
            var lookup = new Lookup();
            ctx.body = utility.prepare_response(utility.SUCCESS,await lookup.get({id:ctx.params.lookup_id}));
        }catch(e){
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE,e);
        }
    },

    list: async function(ctx, next){
        try{
            let query_params = {}
            _.defaults(query_params, ctx.query);
            var lookup = new Lookup();
            ctx.body = utility.prepare_collection(utility.SUCCESS,await lookup.list(query_params));
        }catch(e){
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE,e);
        }
    },

    update: async function(ctx, next){
        try{
            var lookup = new Lookup(ctx.request.body);
            ctx.body = utility.prepare_response(utility.SUCCESS,await lookup.update({id:ctx.params.lookup_id}));
        }catch(e){
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE,e);
        }
    },

    create: async function(ctx, next){
        try{
            var lookup = new Lookup(ctx.request.body);
            ctx.body = utility.prepare_response(utility.SUCCESS,await lookup.create({id_prefix:'lookup'}));
        }catch(e){
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE,e);
        }
    },
    delete: async function(ctx, next){
        try{
            var lookup = new Lookup();
            ctx.body = utility.prepare_response(utility.SUCCESS,await lookup.delete({id:ctx.params.lookup_id, hard: true}));
        }catch(e){
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE,e);
        }
    },

    validate_insert: async function(ctx, next){
        try{
            var rules = {
                //email: 'email'
            };
            var messages = {
                //email: "Invalid Email"
            };
            var filters = {
                before: {
                    //email: "lowercase|trim"
                }
            }
            await co(ctx.validateBody(rules, messages, filters));
            if (ctx.validationErrors) {
                ctx.status = 422;
                ctx.body = ctx.validationErrors;
            } else {
                await next();
            }
        }catch(e){
            console.log(e.stack);
            ctx.status = 400;
            ctx.body = utility.prepare_response(utility.FAILURE, e);
        }

    },

    validate_update: async function(ctx, next){
        console.log(ctx.request.body)
        var rules = {
            //email: 'email'
        };
        var messages = {
            //email: "Invalid Email"
        };
        var filters = {
            before: {
                //email: "lowercase|trim"
            }
        }

        await co(ctx.validateBody(rules, messages, filters));

        if (ctx.validationErrors) {
            ctx.status = 422;
            ctx.body = ctx.validationErrors;
        } else {
            await next();
        }
    },
}

module.exports = object;