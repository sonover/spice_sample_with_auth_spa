"use strict";
import User from '../models/User';
import Token from '../models/Token';
import Welcome from '../mail/Welcome';
import PasswordReset from '../mail/PasswordReset';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
let utility = spice.classes.RestHelper;
import co from 'co';


var object = {
	get: async function (ctx, next){
		try{
			var user = new User();
			ctx.body = utility.prepare_response(utility.SUCCESS,await user.get({id:ctx.params.user_id}));
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE,e);
		}
	},

	authenticate: async function (ctx, next){
		try{
			var user = new User();
			let user_obj = await user.authenticate(ctx.request.body);
			var token = jwt.sign({id:user_obj.id, group:user_obj.group.id}, spice.config.jwt.secret);
			ctx.body = utility.prepare_response(utility.SUCCESS, user_obj,"",token);
		}catch(e){
			if(e.message == 'unauthenticated'){
				ctx.status = 401;
			}else{
				ctx.status = 400;
			}
			ctx.body = utility.prepare_response(utility.FAILURE, "Authentication Failed");
		}
	},


	send_reset_password_email: async function(ctx, next){
		try{
			var user = new User();
			let user_obj = await user.getByEmail({email:ctx.request.body.email});
			//delete old tokens
			let get_token = new Token();
			let tokens  = await get_token.list({query:`reference="${user_obj.id}"`});
			if(tokens.length>0){
				for(var t of tokens){
					await get_token.delete({id:t.id});
				}
			}
			
			let token = new Token({reference:user_obj.id});
			let token_params = {id_prefix:'t'};
			if(spice.config.spice.password_reset_expires){
				token_params.expiry = spice.config.spice.password_reset_expiry_period||1800;
			}
			console.log("Creating token", token_params);
			let token_obj = await token.create(token_params);
			console.log("created token: ", token_obj);

			let password_reset = new PasswordReset({});
			let mail_response = await password_reset.send({
				data:{user:user_obj,token:token_obj.id, return_url:spice.config.spice.password_reset_return_url+token_obj.id},
				options: {
					sender: 'chad@sonover.com',
					to: ctx.request.body.email,
					subject: "Reset Password"
				}
			});
			ctx.body = utility.prepare_response(utility.SUCCESS, {user:user_obj,token:token_obj.id, return_url:ctx.request.body.return_url+"/"+token_obj.id});
		}catch(e){
			console.log("Error happened:",e)
			ctx.status = 401;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},

	reset_password: async function(ctx, next){
		try{
			console.log("Here 1");
			var user = new User();
			let body = await user.reset_password({email:ctx.request.body.email, old_password:ctx.request.body.old_password, new_password:ctx.request.body.new_password});
			ctx.body = utility.prepare_response(utility.SUCCESS, body);
		}catch(e){
			ctx.status = 401;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},

	check_token: async function(ctx, next){
		try{
			var token = new Token();
			var user = new User();
			let user_obj = await user.getByEmail({email:ctx.request.body.email});
			let token_obj = await token.get({id:ctx.request.body.token});
			if(user_obj.id == token_obj.reference){
				//reset password
				var new_user = await user.reset_password({email:ctx.request.body.email, new_password:ctx.request.body.password})
				await token.delete({id:ctx.request.body.token});
				ctx.body = utility.prepare_response(utility.SUCCESS,new_user);
			}else{
				ctx.body = utility.prepare_response(utility.FAILURE, "token mismatch");
			}
		}catch(e){
			ctx.status = 401;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},

	list: async function(ctx, next){
		try{
			let query_params = {}
			_.defaults(query_params, ctx.query);
			var user = new User();
			let collection = await user.list(query_params);
			ctx.body = utility.prepare_collection(utility.SUCCESS, collection);
		}catch(e){
			console.log(e, e.stack);
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},

	update: async function(ctx, next){
		try{
			var user = new User(ctx.request.body);
			ctx.body = utility.prepare_response(utility.SUCCESS,await user.update({id:ctx.params.user_id}));
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},
	create: async function(ctx, next){
		try{
			var user = new User(ctx.request.body);
			let created = await user.create({id_prefix:'user'})
			console.log("After create: ", ctx.request.body);
			if(ctx.request.body.send_welcome && ctx.request.body.send_welcome == true){
				let welcome = new Welcome({});
				let template_data = {
					email:ctx.request.body.email, 
					password:ctx.request.body.password,
					return_url: spice.config.spice.welcome_return_url
				};
				console.log(template_data);
				let mail_response = await welcome.send({
					data:template_data,
					options: {
						sender: 'chad@sonover.com',
						to: ctx.request.body.email,
						subject: "Welcome"
					}
				});
			}
			ctx.body = utility.prepare_response(utility.SUCCESS, created);
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},
	delete: async function(ctx, next){
		try{
			var user = new User();
			let delete_response = await user.delete({id:ctx.params.user_id, hard: true});
			ctx.body = utility.prepare_response(utility.SUCCESS,delete_response);
		}catch(e){
			ctx.status = 400;
			ctx.body = utility.prepare_response(utility.FAILURE, e);
		}
	},

	validate_email: async function(ctx, next){
		console.log(ctx.request.body)
		var rules = {
			return_url: 'required',
			email: 'required|email|emailMustMatchUser'
		};
		var messages = {
			email: "Invalid Email"
		};
		var filters = {
			before: {
				email: "lowercase|trim",
				url: "trim"
			}
		}

		await ctx.validateBody(rules, messages, filters);

		if (ctx.validationErrors) {
			ctx.status = 422;
			ctx.body = ctx.validationErrors;
		} else {
			await next();
		}
	},

	validate_password_reset: async function(ctx, next){
		console.log(ctx.request.body)
		var rules = {
			token: 'required',
			email: 'required|email',
			new_password: 'required'
		};
		var messages = {
			email: "Invalid Email"
		};
		var filters = {
			before: {
				email: "lowercase|trim",
				url: "trim"
			}
		}

		await ctx.validateBody(rules, messages, filters);

		if (ctx.validationErrors) {
			ctx.status = 422;
			ctx.body = ctx.validationErrors;
		} else {
			await next();
		}
	},

	validate_reset_password: async function(ctx, next){
		console.log(ctx.request.body)
		var rules = {
			email: 'required|email',
			old_password: 'required',
			new_password: 'required'
		};
		var messages = {
			email: "Invalid Email"
		};
		var filters = {
			before: {
				email: "lowercase|trim",
				url: "trim"
			}
		}
		console.log("Before validate");
		await ctx.validateBody(rules, messages, filters);
		console.log("After validation", ctx.validationErrors)
		if (ctx.validationErrors) {
			ctx.status = 422;
			ctx.body = ctx.validationErrors;
		} else {
			await next();
		}
	},

	validate_insert: async function(ctx,next){
		try{
			var rules = {
				first_name: 'required',
				password: 'required|between:2,15',
				last_name: 'required',
				email: 'required|email|emailIsUnique',
				group: 'required|usergroupShouldExist'
			};
			var messages = {
				email: "Invalid Email"
			};
			var filters = {
				before: {
					email: "lowercase|trim",
					password: "trim"
				}
			}

			await co(ctx.validateBody(rules, messages, filters));
			if (ctx.validationErrors) {
				ctx.status = 422;
				ctx.body = ctx.validationErrors;
			} else {
				await next();
			}
		}catch(e){
			console.log(e.stack);
		}
	},

	validate_update: async function(ctx, next){
		console.log(ctx.request.body)
		var rules = {
			password: 'between:2,15',
			email: 'email'
		};
		var messages = {
			email: "Invalid Email"
		};
		var filters = {
			before: {
				email: "lowercase|trim",
				password: "trim"
			}
		}

		await co(ctx.validateBody(rules, messages, filters));

		if (ctx.validationErrors) {
			ctx.status = 422;
			ctx.body = ctx.validationErrors;
		} else {
			await next();
		}
	},

	validate_authentication: async function(ctx, next){
		var rules = {
			email: 'required|email',
			password: 'required',
		};
		var messages = {

		};
		var filters = {
			before: {
				email: "lowercase|trim",
				password: "trim"
			}
		}

		await co(ctx.validateBody(rules, messages, filters));

		if (ctx.validationErrors) {
			ctx.status = 401;
			let errors = []
			for(var i of ctx.validationErrors){
				errors.push(_.values(i)[0]);
			}
			ctx.body = errors;
		} else {
			await next();
		}
	}
}

module.exports = object;