"use strict";

const props = {
    reference:{type:"string", defaults:{read:""}}
}
export default class Token extends spice.classes.Model{
	constructor(args={}){
		super({connection:'default', props:props, args:args});
		this.type = 'token';
	}

	hooks(){
		return{
			create:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}

				}
			},
			get:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			},
			update:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			},
			delete:{
				before:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				},
				after:async (data)=>{
					try{
						//your code comes here
						return data;
					}catch(e){
						throw new Error(e);
					}
				}
			}
		}
	}

	serializer(){
		return {
			read: {
				defaults: {

				},
				modifiers:[
					/*async (data)=>{
						try{
							if(_.isArray(data)){
								//modifiers for arrays

							}else{

								//modifiers for single
							}
							return data;
						}catch(e){
							throw new Error(e);
						}
					}*/
				],
				cleaners:[
					//this is the default cleaner is none is specifier. add cleaner to override the default
					/*async (data)=>{
						try{
							if(_.isArray(data)){
								for(let i of data){
									delete i['deleted'];
									delete i['type'];
								}
							}else{
								delete data['deleted'];
								delete data['type'];
							}
							return data;
						}catch(e){
							throw new Error(e);
						}
					}*/
				]
			},
			write:{
				defaults: {

				},
				modifiers:[

				],
				cleaners:[

				]
			}
		}
	}
}