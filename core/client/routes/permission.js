"use strict";
import Router from 'koa-router';
import jwt from 'koa-jwt';
import convert from 'koa-convert';
var jwt_controller = require('../controllers/jwt');

let router = new Router();
var permission = require('../controllers/permission');
router.prefix('/api/permissions');
router.use(convert(jwt({ secret: spice.config.jwt.secret})));
router.use(jwt_controller.check_user_exist);

router.get('/', permission.list);
router.post('/', permission.validate_insert, permission.create);
router.get('/:permission_id', permission.get);
router.put('/:permission_id', permission.validate_update, permission.update);
router.delete('/:permission_id', permission.delete);

module.exports = router;