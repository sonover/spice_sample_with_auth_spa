/**
 * Created by chadfraser on 27/02/2017.
 */

import login from './pages/login.vue';
import forget_password from './pages/forget_password.vue';
import home from './pages/home.vue';
import page1 from './pages/sample_page1.vue';
import page2 from './pages/sample_page2.vue';
import profile from './pages/profile.vue';
import setting from './pages/setting.vue';
import reset from './pages/resetpassword.vue';
import resetrequest from './pages/resetpasswordrequest.vue';
import users from './components/settings/user';
import user_groups from './components/settings/group';
import permissions from './components/settings/permission';
import lookups from './components/settings/lookup';

export default [
    {
        path:'/', 
        component: home, 
        name: 'home', 
        meta:{
            private:true
        }
    },
    {
        path:'/sample1', 
        component: page1, 
        name: 'sample1', 
        meta:{
            private:true
        }
    },
    {
        path:'/sample2', 
        component: page2, 
        name: 'sample2', 
        meta:{
            private:true,
            permissions:['can_load_sample_2']
        }
    },
    {
        path:'/users/reset/:token', 
        component: forget_password, 
        name: 'forget_password', 
        meta:{
            private:false
        }
    },
    {
        path:'/reset', 
        component: reset, 
        name: 'reset', 
        meta:{
            private:false
        }
    },
    {
        path:'/resetrequest', 
        component: resetrequest, 
        name: 'resetrequest', 
        meta:{
            private:false
        }
    },
    {
        path:'/setting', 
        component: setting, 
        name: 'setting', 
        meta:{
            private:true
        }
    },
    {
        path:'/login', 
        component: login, 
        name: 'login', 
        meta:{
            isLogin:true
        }
    },
    {
        path:'/profile', 
        component: profile, 
        name: 'profile', 
        meta:{
            private:true
        }
    }
];