window._ = require('lodash');

window.axios = require('axios');

window.moment = require('moment');
if(JSON.parse(window.localStorage.getItem('auth_user')))
window.permissions = JSON.parse(window.localStorage.getItem('auth_user')).data.group.permissions;

window.Vue = require('vue');
import User from './api/User'

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

window.loadUser = async ()=>{
    let user = new User();
    if(window.localStorage.getItem('auth_user')){
        let response = await user.get(JSON.parse(window.localStorage.getItem('auth_user')).data.id);
        window.localStorage.setItem("permissions",response.data.group.permissions);
        window.permissions = response.data.group.permissions;
        await window.updateUser(response.data);
    }
}

window.updateUser = async (data)=>{
    let user = new User();
    let user_obj = JSON.parse(window.localStorage.getItem('auth_user'));
    if(user_obj){
        if(data){
            user_obj.data = data;
        }else{
            let response = await user.get(user_obj.data.id);
            user_obj.data = response.data;
        }
        window.localStorage.setItem("auth_user",JSON.stringify(user_obj));
    }
}

window.hasPermission = (permission)=>{
    let response = _.find(permissions, o=>{
        return o.id == permission
    });
    return response != undefined;
}

// For when browser refreshed
if(window.localStorage.getItem('auth_user')){
    window.axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(window.localStorage.getItem('auth_user')).token}`;
    window.loadUser();
}

window.Event = new Vue({});

