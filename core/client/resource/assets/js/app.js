
require('./bootstrap');
import '../scss/app.scss';
import {
    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    Card,
    Input,
    Loading,
    Dialog,
    Tabs,
    Table,
    TableColumn,
    Switch,
    Select,
    Option,
    OptionGroup,
    Popover,
    Tooltip,
    TabPane,
    Button,
    Message,
    MessageBox,
    Notification
} from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

// configure language
locale.use(lang)

/* import io from 'socket.io-client' */

import VueRouter from 'vue-router';
import Vue2Filters from 'vue2-filters'
import config from '../../../../../config/client';
import routes from './routes';
import App from './App.vue';
import store from './store/store';
import Gravatar from 'vue-gravatar';

window.config = config;

Vue.component('v-gravatar', Gravatar);
//element imports
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)
Vue.use(Input);
Vue.use(Button);
Vue.use(Dialog);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Switch)
Vue.use(Select)
Vue.use(Option)
Vue.use(OptionGroup)
Vue.use(Card)
Vue.use(Popover)
Vue.use(Tooltip)
Vue.use(Loading.directive)
Vue.prototype.$loading = Loading.service
Vue.prototype.$message = Message
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification

Vue.use(VueRouter);
Vue.use(Vue2Filters)

Vue.component('app',App);
/* Vue.use(ElementUI, { locale }); */
/* Vue.http.interceptors.push(function(request, next) {
    try {
        let header_obj = config.auth_header();
        for (let i in header_obj) {
            request.headers.set(i, header_obj[i]);
        }
        next(function (response) {
            if (response.status == 403) {
                window.localStorage.removeItem('auth_user');
                this.$store.dispatch('set_user_object', null);
                navigate('login');
            }
        });
    }catch(e){
        console.log(e.stack);
    }
}); */

const router = new VueRouter({routes, mode:'history'});

router.beforeEach(async (to, from, next)=>{
   
    try {
        await loadUser();
        let auth_user = JSON.parse(window.localStorage.getItem('auth_user'));

        //check route permissions
        let permission_res = [{has: true}]
        if(to.meta.permissions){
            permission_res = _.map(to.meta.permissions, p=>{
                return {has:hasPermission(p)};
            })
        }
        if(_.every(permission_res, ['has', true])){
            if(to.meta.private){
                if(auth_user && auth_user.token){// routes user to login screen if its a private page and they are not authenticated
                    if(auth_user.data.reset_required == "true" || auth_user.data.reset_required == true){
                        next({name:'reset'});
                    }else{
                        next();
                    }
                }else{
                    next({name:'login'});
                }
            }else if(to.meta.isLogin && auth_user){//avoid logged in user from going to login page
                next(from);
            }else{
                next();
            }
        }else{
            vue.$message({
                message: 'ERROR, this page is restricted.',
                type: 'error'
            })
        }
    }catch(e){
        console.log(e.stack);
    }
})

let vue = new Vue({
    el: '#spice',
    router,store
});

window.vue = vue;



