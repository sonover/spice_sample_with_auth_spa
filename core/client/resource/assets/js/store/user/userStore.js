const state = {
    authUser: null
}

const getters = {
    getAuthUser: state=>{
        return state.authUser;
    }
}

const mutations = {
    set_auth_user(state, userObject){
        state.authUser = userObject;
    }
}

const actions = {
    set_user_object:({commit}, userObject)=>{
        commit('set_auth_user', userObject)
    }
}

export default {
    state, mutations, actions, getters
}