const state = {
    permissions: []
}

const getters = {
    getPermissions: state=>{
        return state.permissions;
    }
}

const mutations = {
    set_permission(state, permissionArray){
        state.permissions = permissionArray;
    }
}

const actions = {
    set_permission:({commit}, permissionArray)=>{
        commit('set_permission', permissionArray)
    }
}

export default {
    state, mutations, actions, getters
}