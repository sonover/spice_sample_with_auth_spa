/**
 * Created by chadfraser on 27/02/2017.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import userStore from './user/userStore';
Vue.use(Vuex);

const debug = process.NODE_ENV !== 'production';
export default new Vuex.Store({
    modules: {userStore},
    strict: debug
})