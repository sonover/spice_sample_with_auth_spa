"use strict";
import Service from './Service'
export default class User extends Service {
    constructor() {
        super({
            resource: "users"
        })
    }

    async authenticate(auth_data){
        try{
            //console.log(this.path+"----"+this.resource);
            let response = await axios.post(this.endpoint+"/authenticate", auth_data);
            console.log("response:", response)
            if(response.status >= 200 && response.status <= 400){
                return response.data;
            }
            throw new Error(response.statusText);
        }catch(e){
            throw e;
        }
    }

    async send_reset_password(auth_data){
        try{
            let response = await axios.post(this.endpoint+"/password/reset", auth_data);
            console.log("response:", response)
            if(response.status >= 200 && response.status <= 400){
                return response.data;
            }
            throw new Error(response.statusText);
        }catch(e){
            throw e;
        }
    }

    async reset_password(obj){
        try{
            let response = await axios.post(this.endpoint+"/reset_password", obj);
            console.log("response:", response)
            if(response.status >= 200 && response.status <= 400){
                return response.data;
            }
            throw new Error(response.statusText);
        }catch(e){
            throw e;
        }
    }

    async send_reset_password_request(email){
        try{
            let response = await axios.post(this.endpoint+"/password/email", {email: email});
            console.log("response:", response)
            if(response.status >= 200 && response.status <= 400){
                return response.data;
            }
            throw new Error(response.statusText);
        }catch(e){
            throw e;
        }
    }
}