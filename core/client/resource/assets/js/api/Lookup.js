import Service from './Service'
export default class Lookup extends Service {
    constructor() {
        super({
            resource: "lookups"
        })
    }

    async list_by_category(category){
        return this.list({query:`category="${category}"`});

    }
}