export default class Service{
    constructor(args) {
        this.resource = args.resource;
        this.endpoint = '/api/'+ this.resource;
    }

    async create(data) {
        let response =  await axios.post(this.endpoint, data);
        return response;
    }

    async list(params){
        let response = await axios.get(this.endpoint, {
            params: params
        })
        return response.data;
    }

    async get(id){
        let response = await axios.get(this.endpoint + '/' + id);
        return response.data;
    }

    async update(id, data){
        let response =  await axios.put(this.endpoint + '/' + id, data);
        return response.data;
    }

    async delete(id){
        let response =  await axios.delete(this.endpoint + '/' + id);
        return response.data;
    }
}