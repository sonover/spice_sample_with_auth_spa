"use strict";
import Service from './Service'
export default class UserGroup extends Service {
    constructor() {
        super({
            resource: "user_groups"
        })
    }
}