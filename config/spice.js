/**
 * Created by chadfraser on 13/02/2017.
 */
module.exports = {
    appId: "com.sonover.spice",
    port: process.env.PORT,
    enable_socket: false,
    auth_header: ()=>{
        return {Authorization: `Bearer ${window.localStorage.getItem('token')}`}
    },
    password_reset_expires: false,
    password_reset_expiry_period: 1000*60*60,
    password_reset_return_url: `http://localhost:${process.env.PORT}/users/reset/`,
    welcome_return_url: `http://localhost:${process.env.PORT}`
}