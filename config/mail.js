module.exports = {
    default_driver: 'file',
    drivers: {
        debug: {

        },
        file: {
            
        },
        sendgrid: {
            key: process.env.SENDGRID_KEY
        }
    },
    providers: {//import prividers
        debug: spice.classes.mail.providers.Debug,
        sendgrid: require('spice_mail_sendgrid'),
        file: spice.classes.mail.providers.File
    }
};

