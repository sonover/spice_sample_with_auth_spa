let api_protocal = "http";
let api_host = "localhost";
let api_base_path = "/api";
export default {
    api_base_path: api_base_path,
    auth_path: "users/authenticate",
    auth_header: ()=>{
        let obj = {};
        if(window.localStorage.getItem('auth_user')){
            obj['Authorization'] = `Bearer ${JSON.parse(window.localStorage.getItem('auth_user')).token}`
        }
        obj['Content-Type'] = 'application/x-www-form-urlencoded';
        return obj;
    }
}