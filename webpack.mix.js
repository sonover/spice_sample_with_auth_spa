let { mix } = require('laravel-mix');


mix.js('core/client/resource/assets/js/app.js', 'public/js')
    .sass('core/client/resource/assets/scss/app.scss', 'public/css')
    .disableNotifications()
    .sourceMaps()
    .extract([
        "babel-polyfill",
        'vue'
    ])
    .autoload({

    })
    .setPublicPath('public')
    .browserSync({
        proxy: 'http://localhost:3005',
        files: ['public/**/*.css', 'public/**/*.js']
    });